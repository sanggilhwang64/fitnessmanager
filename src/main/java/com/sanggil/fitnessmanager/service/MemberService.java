package com.sanggil.fitnessmanager.service;

import com.sanggil.fitnessmanager.entity.Member;
import com.sanggil.fitnessmanager.model.MemberItem;
import com.sanggil.fitnessmanager.model.MemberResponse;
import com.sanggil.fitnessmanager.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public void setMember(String memberName, String memberPhone, Float height, Float weight, Float goalWeight) {
        Member addData = new Member();
        addData.setMemberName(memberName);
        addData.setMemberPhone(memberPhone);
        addData.setDateLast(LocalDate.now());
        addData.setHeight(height);
        addData.setWeight(weight);
        addData.setGoalWeight(goalWeight);

        memberRepository.save(addData);
    }

    public List<MemberItem> getMembers() {
        List<Member> originList = memberRepository.findAll();

        List<MemberItem> result = new LinkedList<>();

        for (Member item : originList) {
            MemberItem addItem = new MemberItem();
            addItem.setId(item.getId());
            addItem.setMemberName(item.getMemberName());
            addItem.setMemberPhone(item.getMemberPhone());
            addItem.setDateLast(item.getDateLast());
            addItem.setHeight(item.getHeight());

            result.add(addItem);
        }

        return result;
    }

    public MemberResponse getMember(long id) {
        Member originData = memberRepository.findById(id).orElseThrow();

        MemberResponse result = new MemberResponse();
        result.setId(originData.getId());
        result.setMemberName(originData.getMemberName());
        result.setMemberPhone(originData.getMemberPhone());
        result.setDateLast(originData.getDateLast());
        result.setHeight(originData.getHeight());
        result.setWeightBody(originData.getWeight() + "/" + originData.getGoalWeight());

        return result;
    }

}
