package com.sanggil.fitnessmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class MemberResponse {
    private Long id;
    private String memberName;
    private String memberPhone;
    private LocalDate dateLast;
    private Float height;
    private String weightBody;
}
