package com.sanggil.fitnessmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String memberName;

    @NotNull
    @Length(min = 11, max = 20)
    private String memberPhone;

    @NotNull
    private Float height;

    @NotNull
    private Float weight;

    @NotNull
    private Float goalWeight;
}
