package com.sanggil.fitnessmanager.repository;

import com.sanggil.fitnessmanager.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
