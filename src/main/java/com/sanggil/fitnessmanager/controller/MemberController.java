package com.sanggil.fitnessmanager.controller;

import com.sanggil.fitnessmanager.model.MemberItem;
import com.sanggil.fitnessmanager.model.MemberRequest;
import com.sanggil.fitnessmanager.model.MemberResponse;
import com.sanggil.fitnessmanager.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/data")
    public String setMember(@RequestBody @Valid MemberRequest request) {
        memberService.setMember(
                request.getMemberName(),
                request.getMemberPhone(),
                request.getHeight(),
                request.getWeight(),
                request.getGoalWeight()
        );
        return "OK";
    }

    @GetMapping("/all")
    public List<MemberItem> getMembers() {
        List<MemberItem> result = memberService.getMembers();

        return result;
    }

    @GetMapping("/data/id/{id}")
    public MemberResponse getMember(@PathVariable long id) {
        MemberResponse result = memberService.getMember(id);
        return result;
    }

}
